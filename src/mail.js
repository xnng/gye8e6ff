const db = require('./db/connect')
const mailin = require('mailin')

const start = () => {
  mailin.start({
    host: '0.0.0.0',
    port: 25,
    disableWebhook: true
  })

  mailin.on('error', (err) => {
    console.log(err)
  })

  mailin.on('message', function (_, data) {
    if (data.html) {
      db.query('insert into mail(username,cname,user_from,subject,html,text) values(?,?,?,?,?,?)', [
        data.to[0].address.split('@')[0],
        data.to[0].address.split('@')[1],
        data.from[0].address,
        data.subject,
        data.html,
        data.text
      ]).catch((err) => {
        console.log(err.stack)
      })
    }
  })
}

module.exports = start
